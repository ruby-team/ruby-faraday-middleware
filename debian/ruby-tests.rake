require 'rspec/core/rake_task'
# Failing tests are disabled
RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern      = FileList['./spec/**/*_spec.rb'] - 
    FileList['./spec/unit/caching_spec.rb','./spec/unit/gzip_spec.rb']
end

task :default => :spec
